from twisted.web import proxy, http
from twisted.internet import reactor
import re


class ProxyClient(proxy.ProxyClient):

    def handleHeader(self, key, value):
        # print(key, ' ', value)
        proxy.ProxyClient.handleHeader(self, key, value)

    def handleResponsePart(self, buffer):
        # regex = re.compile(b'(?<=href=").*?(?=")')
        # buffer = re.sub(regex, b'http://google.com', buffer)
        proxy.ProxyClient.handleResponsePart(self, buffer)


class ProxyClientFactory(proxy.ProxyClientFactory):
     protocol = ProxyClient


class ProxyRequest(proxy.ProxyRequest):
    protocols = {b'http': ProxyClientFactory}

    def process(self):
        url_host = self.getAllHeaders()[b'host'].decode("utf-8")
        print(url_host)
        proxy.ProxyRequest.process(self)


class Proxy(proxy.Proxy):
     requestFactory = ProxyRequest


class ProxyFactory(http.HTTPFactory):
    protocol = Proxy


# log.startLogging(sys.stdout)
reactor.listenTCP(8100, ProxyFactory())
reactor.run()
